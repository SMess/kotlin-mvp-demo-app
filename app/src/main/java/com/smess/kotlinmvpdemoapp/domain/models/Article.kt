package com.smess.kotlinmvpdemoapp.domain.models

import android.arch.persistence.room.ColumnInfo
import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey
import java.io.Serializable

/**
 * Created by samy.messara
 */
@Entity(tableName = "articles")
class Article(@ColumnInfo(name = "albumId") val albumId: Int,
              @PrimaryKey(autoGenerate = false) val id: Int,
              @ColumnInfo(name = "title") val title: String,
              @ColumnInfo(name = "url") val url: String,
              @ColumnInfo(name = "thumbnailUrl") val thumbnailUrl: String) : Serializable {

    override fun toString(): String {
        return "Article(albumId=$albumId, id=$id, title='$title', url='$url', thumbnailUrl='$thumbnailUrl')"
    }
}
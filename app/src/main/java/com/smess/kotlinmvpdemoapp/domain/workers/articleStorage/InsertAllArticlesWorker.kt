package com.smess.kotlinmvpdemoapp.domain.workers.articleStorage

import com.smess.basemvpandroidarchitecture.workers.AbstractWorker
import com.smess.kotlinmvpdemoapp.domain.models.Article
import com.smess.kotlinmvpdemoapp.storage.article.ArticlesDatabase

/**
 * Created by samy.messara
 */
class InsertAllArticlesWorker constructor(private val database: ArticlesDatabase, private val articleList: ArrayList<Article>, private val insertAllArticlesWorkerCallback: InsertAllArticlesWorkerCallback, private val tag: String) : AbstractWorker() {

    override fun run() {
        database.articleStorageDao().clearTable()

        database.articleStorageDao().insertAllArticles(articleList)
        insertAllArticlesWorkerCallback.onInsertAllArticlesSuccess(tag)
    }

}

package com.smess.kotlinmvpdemoapp.domain.workers.articleNetwork

import com.smess.basemvpandroidarchitecture.workers.AbstractWorker
import com.smess.kotlinmvpdemoapp.services.article.ArticleServices
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers

/**
 * Created by samy.messara
 */

class ArticleWorker(private val callback: ArticleWorkerCallback, private val tag: String) : AbstractWorker() {

    private var disposable: Disposable? = null

    private val articleServices by lazy {
        ArticleServices.create()
    }

    override fun run() {
        //Make async stuff here

        disposable = articleServices.getArticles()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                        { result ->
                            callback.onFetchDataSuccess(result, tag)
                            disposable?.dispose()
                        },
                        { error ->
                            callback.onWorkerError(error, tag)
                            disposable?.dispose()
                        }
                )

    }

}
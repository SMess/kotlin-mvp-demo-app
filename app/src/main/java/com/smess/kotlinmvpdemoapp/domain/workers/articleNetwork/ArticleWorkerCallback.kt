package com.smess.kotlinmvpdemoapp.domain.workers.articleNetwork

import com.smess.basemvpandroidarchitecture.workers.BaseWorkerCallback
import com.smess.kotlinmvpdemoapp.domain.models.Article

/**
 * Created by samy.messara
 */

interface ArticleWorkerCallback : BaseWorkerCallback {
    fun onFetchDataSuccess(listArticles: ArrayList<Article>, tag: String)
}
package com.smess.kotlinmvpdemoapp.domain.workers.articleStorage

import com.smess.basemvpandroidarchitecture.workers.BaseWorkerCallback
import com.smess.kotlinmvpdemoapp.domain.models.Article


/**
 * Created by samy.messara
 */
interface GetAllArticlesWorkerCallback : BaseWorkerCallback {
    fun onGetArticlesListFromDbSuccess(listArticles: ArrayList<Article>, tag: String)
}
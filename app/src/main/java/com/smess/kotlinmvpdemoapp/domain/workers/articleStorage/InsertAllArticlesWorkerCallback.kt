package com.smess.kotlinmvpdemoapp.domain.workers.articleStorage

import com.smess.basemvpandroidarchitecture.workers.BaseWorkerCallback


/**
 * Created by samy.messara
 */
interface InsertAllArticlesWorkerCallback : BaseWorkerCallback {
    fun onInsertAllArticlesSuccess(tag: String)
}
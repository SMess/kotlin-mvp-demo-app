package com.smess.kotlinmvpdemoapp.domain.workers.articleStorage

import com.smess.basemvpandroidarchitecture.workers.AbstractWorker
import com.smess.kotlinmvpdemoapp.domain.models.Article
import com.smess.kotlinmvpdemoapp.storage.article.ArticlesDatabase
import java.util.*

/**
 * Created by samy.messara
 */
class GetAllArticlesWorker constructor(private val database: ArticlesDatabase, private val getAllArticlesWorkerCallback: GetAllArticlesWorkerCallback, private val tag: String) : AbstractWorker() {

    private var listArticles = ArrayList<Article>()

    override fun run() {
        listArticles.clear()
        listArticles.addAll(database.articleStorageDao().getAllArticles())

        if (listArticles.size == 0) {
            val error = Throwable("No data to show")
            getAllArticlesWorkerCallback.onWorkerError(error, tag)
        } else {
            getAllArticlesWorkerCallback.onGetArticlesListFromDbSuccess(listArticles, tag)
        }
    }
}

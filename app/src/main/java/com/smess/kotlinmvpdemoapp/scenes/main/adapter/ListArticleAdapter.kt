package com.smess.kotlinmvpdemoapp.scenes.main.adapter

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.smess.kotlinmvpdemoapp.R
import com.smess.kotlinmvpdemoapp.domain.models.Article
import kotlinx.android.synthetic.main.item_list_article.view.*




/**
 * Created by samy.messara
 */
class ListArticleAdapter(private val items: List<Article>, private val listenerItemClick: (Article) -> Unit) : RecyclerView.Adapter<ListArticleAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        var inflatedView = LayoutInflater.from(parent.context).inflate(R.layout.item_list_article, parent, false)
        return ViewHolder(inflatedView)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) = holder.bind(items[position], listenerItemClick)

    override fun getItemCount() = items.size

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bind(article: Article, listenerItemClick: (Article) -> Unit) = with(itemView) {

            tvTitle.text = article.title

            Glide.with(this).load(article.thumbnailUrl)
                    .apply(RequestOptions().placeholder(R.drawable.no_default_thumbnail).error(R.drawable.no_default_thumbnail))
                    .into(ivArticleThumbnail)

            //Listener for all view click
            ivArticleThumbnail.setOnClickListener { listenerItemClick(article) }
        }
    }
}
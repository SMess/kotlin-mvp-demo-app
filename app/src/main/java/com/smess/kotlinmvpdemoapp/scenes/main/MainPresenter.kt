package com.smess.kotlinmvpdemoapp.scenes.main

import android.content.Context
import android.os.Bundle
import android.support.v4.widget.SwipeRefreshLayout
import android.support.v7.widget.RecyclerView
import android.util.Log
import com.smess.basemvpandroidarchitecture.ui.BasePresenter
import com.smess.kotlinmvpdemoapp.domain.models.Article
import com.smess.kotlinmvpdemoapp.scenes.main.adapter.ListArticleAdapter

class MainPresenter(viewContract: MainContract.ViewContract) : BasePresenter<MainContract.ViewContract, MainContract.InteractorContract>(viewContract),
        MainContract.PresenterContract.View,
        MainContract.PresenterContract.Interactor,
        SwipeRefreshLayout.OnRefreshListener {

    companion object {
        private val TAG = MainPresenter::class.java.simpleName
    }

    private var context: Context? = null
    private var isRefresh: Boolean = false

    override fun createInteractor(P: BasePresenter<MainContract.ViewContract, MainContract.InteractorContract>): MainContract.InteractorContract {
        return MainInteractor(this)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onResume() {
        super.onResume()
    }

    override fun setContext(applicationContext: Context?) {
        context = applicationContext
        interactor.setContext(context)
    }

    override fun loadData() {
        Log.d(TAG, "loadData")

        if (!isRefresh) {
            view.showProgress()
        }
        interactor.getListArticles()
    }

    //Called from MainInteractor on fetchDataSuccess
    override fun handleListArticles(listArticles: ArrayList<Article>) {
        Log.d(TAG, "handleListArticles")

        val adapter = ListArticleAdapter(listArticles,
                listenerItemClick = {
                    //go to detail view
                    view.goToArticleDetailActivity(it)
                })

        //Ask the view to update rv content
        view.refreshListArticlesContent(adapter)
        view.showListArticles()


        //Update the main view stats
        if (!isRefresh) {
            view.hideProgress()
        } else {
            view.stopRefreshing()
        }
        view.hideNoDataLayout()
        isRefresh = false
    }

    override fun onRefresh() {
        //On swipe to refresh data
        isRefresh = true
        loadData()
    }

    override fun onFetchDataError(error: String) {
        Log.e(TAG, "onFetchDataError")

        //handle errors
        isRefresh = false
        view.stopRefreshing()
        view.hideProgress()
        view.hideListArticles()
        view.showNoDataLayout()
    }

    override fun onNoNetworkAvailable(noNetworkErrorMessage: String) {
        Log.e(TAG, "onNoNetworkAvailable")

        view.showSnackBarMessage(noNetworkErrorMessage)
    }

    override fun onClickFabScrollTop() {
        view.smoothScrollToTop()
        view.hideFab()
    }

    override fun onScrollStateChanged(recyclerView: RecyclerView) {
        if (recyclerView.canScrollVertically(-1)) {
            view.showFab()
        }else {
            view.hideFab()
        }
    }
}
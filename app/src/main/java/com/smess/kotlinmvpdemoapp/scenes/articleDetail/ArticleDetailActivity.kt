package com.smess.kotlinmvpdemoapp.scenes.articleDetail

import android.annotation.SuppressLint
import android.os.Bundle
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.smess.basemvpandroidarchitecture.ui.BaseActivity
import com.smess.kotlinmvpdemoapp.R
import com.smess.kotlinmvpdemoapp.domain.models.Article
import kotlinx.android.synthetic.main.activity_article_detail.*

/**
 * Created by samy.messara
 */

class ArticleDetailActivity : BaseActivity<ArticleDetailContract.PresenterContract.View>(), ArticleDetailContract.ViewContract {

    companion object {
        private val TAG = ArticleDetailActivity::class.java.simpleName
        val ARG_ARTICLE = "article"
    }

    @SuppressLint("MissingSuperCall")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_article_detail)

        if (intent.hasExtra(ARG_ARTICLE)) {
            presenter.showArticleDetail(intent.extras[ARG_ARTICLE] as Article)
        }
    }

    override fun createPresenter(): ArticleDetailContract.PresenterContract.View {
        return ArticleDetailPresenter(this)
    }

    override fun setArticleImage(url: String) {
        Glide.with(this).load(url)
                .apply(RequestOptions().placeholder(R.drawable.no_default_thumbnail).error(R.drawable.no_default_thumbnail))
                .into(ivArticleDetail)
    }
}

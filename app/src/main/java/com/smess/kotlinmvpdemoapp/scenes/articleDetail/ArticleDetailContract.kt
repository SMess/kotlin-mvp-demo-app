package com.smess.kotlinmvpdemoapp.scenes.articleDetail

import com.smess.basemvpandroidarchitecture.ui.MVPContract
import com.smess.kotlinmvpdemoapp.domain.models.Article


/**
 * Created by samy.messara
 */

interface ArticleDetailContract {

    interface ViewContract : MVPContract.ViewContract {
        fun setArticleImage(url: String)
        //rien pour le moment
    }

    interface PresenterContract : MVPContract.PresenterContract {
        interface View : PresenterContract {
            fun showArticleDetail(article: Article)
            //rien pour le moment
        }

        interface Interactor : PresenterContract {
            //rien pour le moment
        }
    }

    interface InteractorContract : MVPContract.InteractorContract {
        //rien pour le moment
    }
}

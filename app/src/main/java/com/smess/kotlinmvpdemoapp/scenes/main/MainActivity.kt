package com.smess.kotlinmvpdemoapp.scenes.main


import android.annotation.SuppressLint
import android.content.Intent
import android.os.Bundle
import android.os.Parcelable
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.View
import com.smess.basemvpandroidarchitecture.ui.BaseActivity
import com.smess.kotlinmvpdemoapp.R
import com.smess.kotlinmvpdemoapp.domain.models.Article
import com.smess.kotlinmvpdemoapp.scenes.articleDetail.ArticleDetailActivity
import com.smess.kotlinmvpdemoapp.scenes.main.adapter.ListArticleAdapter
import com.smess.kotlinmvpdemoapp.utils.Utils
import kotlinx.android.synthetic.main.activity_main.*


class MainActivity : BaseActivity<MainContract.PresenterContract.View>(), MainContract.ViewContract {

    companion object {
        private val TAG = MainActivity::class.java.simpleName
        private const val LIST_STATE_KEY = "recycler_list_state"
        private const val FAB_STATE_KEY = "fab_list_state"
    }

    private var listState: Parcelable? = null
    private var layoutManager: LinearLayoutManager = LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false)

    @SuppressLint("MissingSuperCall")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        bindClicks()

        //share context to the associated presenter & interactor
        presenter.setContext(applicationContext)

        //Try to load article data on activity create
        presenter.loadData()


        swipeRefreshMainLayout.setOnRefreshListener(presenter)

        //Add scrollListener to rv to handle Fab visibility
        rvListArticles.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrollStateChanged(recyclerView: RecyclerView, newState: Int) {
                super.onScrollStateChanged(recyclerView, newState)
                presenter.onScrollStateChanged(recyclerView)
            }
        })
    }

    /**
     * All on click listeners will be regrouped here  (Call it right after setContentView)
     */
    private fun bindClicks() {
        fabScrollTop.setOnClickListener {
            presenter.onClickFabScrollTop()
        }
    }

    override fun createPresenter(): MainContract.PresenterContract.View {
        return MainPresenter(this)
    }

    override fun refreshListArticlesContent(adapter: ListArticleAdapter) {
        rvListArticles.layoutManager = layoutManager
        rvListArticles.adapter = adapter
    }

    override fun showProgress() {
        //show progressBar
        llProgressContainer.visibility = View.VISIBLE
    }

    override fun hideProgress() {
        //hide progressBar
        llProgressContainer.visibility = View.GONE
    }

    override fun stopRefreshing() {
        swipeRefreshMainLayout.isRefreshing = false
    }

    override fun showNoDataLayout() {
        tvNoData.visibility = View.VISIBLE
    }

    override fun hideNoDataLayout() {
        tvNoData.visibility = View.GONE
    }

    override fun hideListArticles() {
        rvListArticles.visibility = View.GONE
    }

    override fun showListArticles() {
        rvListArticles.visibility = View.VISIBLE
    }

    override fun showSnackBarMessage(message: String) {
        Utils.displayErrorSnackbar(this, clMainContainer, message, getString(R.string.snackbar_button_ok))
    }

    override fun goToArticleDetailActivity(article: Article) {
        val intent = Intent(this, ArticleDetailActivity::class.java)
        intent.putExtra(ArticleDetailActivity.ARG_ARTICLE, article)
        startActivity(intent)
    }

    override fun onSaveInstanceState(state: Bundle) {
        // Save  states
        listState = this.layoutManager.onSaveInstanceState()
        state.putParcelable(LIST_STATE_KEY, listState)
        state.putInt(FAB_STATE_KEY, fabScrollTop.visibility)

        super.onSaveInstanceState(state)
    }

    override fun onRestoreInstanceState(state: Bundle?) {
        super.onRestoreInstanceState(state)
        // Retrieve saved states
        if (state != null) {
            listState = state.getParcelable(LIST_STATE_KEY)
            fabScrollTop.visibility = state.getInt(FAB_STATE_KEY)
        }
    }

    override fun onResume() {
        super.onResume()
        if (listState != null) {
            this.layoutManager.onRestoreInstanceState(listState)
        }
    }

    override fun hideFab() {
        fabScrollTop.hide()
    }

    override fun showFab() {
        fabScrollTop.show()
    }

    override fun smoothScrollToTop() {
        rvListArticles.scrollToPosition(0)
    }
}

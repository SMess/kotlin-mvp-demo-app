package com.smess.kotlinmvpdemoapp.scenes.articleDetail

import com.smess.basemvpandroidarchitecture.ui.BasePresenter
import com.smess.kotlinmvpdemoapp.domain.models.Article

/**
 * Created by samy.messara
 */

class ArticleDetailPresenter(viewContract: ArticleDetailContract.ViewContract) : BasePresenter<ArticleDetailContract.ViewContract, ArticleDetailContract.InteractorContract>(viewContract),
        ArticleDetailContract.PresenterContract.View,
        ArticleDetailContract.PresenterContract.Interactor {


    override fun createInteractor(P: BasePresenter<ArticleDetailContract.ViewContract, ArticleDetailContract.InteractorContract>): ArticleDetailContract.InteractorContract {
        return ArticleDetailInteractor(this)
    }

    override fun showArticleDetail(article: Article) {
        view.setArticleImage(article.url)
    }
}
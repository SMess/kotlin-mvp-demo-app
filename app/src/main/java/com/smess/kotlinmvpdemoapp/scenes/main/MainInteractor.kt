package com.smess.kotlinmvpdemoapp.scenes.main

import android.content.Context
import android.os.Handler
import android.os.Looper
import android.util.Log
import com.smess.basemvpandroidarchitecture.ui.BaseInteractor
import com.smess.kotlinmvpdemoapp.R
import com.smess.kotlinmvpdemoapp.domain.models.Article
import com.smess.kotlinmvpdemoapp.domain.workers.articleNetwork.ArticleWorker
import com.smess.kotlinmvpdemoapp.domain.workers.articleNetwork.ArticleWorkerCallback
import com.smess.kotlinmvpdemoapp.domain.workers.articleStorage.GetAllArticlesWorker
import com.smess.kotlinmvpdemoapp.domain.workers.articleStorage.GetAllArticlesWorkerCallback
import com.smess.kotlinmvpdemoapp.domain.workers.articleStorage.InsertAllArticlesWorker
import com.smess.kotlinmvpdemoapp.domain.workers.articleStorage.InsertAllArticlesWorkerCallback
import com.smess.kotlinmvpdemoapp.storage.article.ArticlesDatabase
import com.smess.kotlinmvpdemoapp.utils.Utils


class MainInteractor(presenter: MainPresenter) : BaseInteractor<MainContract.PresenterContract.Interactor>(presenter), MainContract.InteractorContract, ArticleWorkerCallback, GetAllArticlesWorkerCallback, InsertAllArticlesWorkerCallback {

    companion object {
        private val TAG = MainInteractor::class.java.simpleName
        private val TAG_GET_LIST_ARTICLES_FROM_REMOTE = TAG + "_get_list_articles_from_remote"
        private val TAG_GET_LIST_ARTICLES_FROM_LOCAL = TAG + "_get_list_articles_from_local"
        private val TAG_INSERT_LIST_ARTICLES_TO_DB = TAG + "_insert_list_articles_to_db"
    }

    private var interactorContext: Context? = null

    override fun setContext(context: Context?) {
        interactorContext = context
    }

    override fun getListArticles() =//Check if the device has internet connection
            if (Utils.isNetworkAvailable(interactorContext!!)) {

                //if yes, make the remote call
                ArticleWorker(this, TAG_GET_LIST_ARTICLES_FROM_REMOTE).execute()
            } else {
                presenter.onNoNetworkAvailable(interactorContext!!.getString(R.string.no_network_available_error_message))

                //try to get the articles from databases if possible
                GetAllArticlesWorker(ArticlesDatabase.getInstance(this.interactorContext)!!, this, TAG_GET_LIST_ARTICLES_FROM_LOCAL).execute()
            }


    override fun onFetchDataSuccess(listArticles: ArrayList<Article>, tag: String) {
        Log.d(tag, "onFetchDataSuccess")

        //Data successfully retrieved from remote
        presenter.handleListArticles(listArticles)

        //store data in local database
        InsertAllArticlesWorker(ArticlesDatabase.getInstance(this.interactorContext)!!, listArticles, this, TAG_INSERT_LIST_ARTICLES_TO_DB).execute()
    }


    override fun onGetArticlesListFromDbSuccess(listArticles: ArrayList<Article>, tag: String) {
        Log.d(tag, "onGetArticlesListFromDbSuccess")

        //Data successfully retrieved from db

        // Get a handler that can be used to post to the main thread
        val mainHandler = Handler(Looper.getMainLooper())
        val runnable = Runnable {
            presenter.handleListArticles(listArticles)
        }
        mainHandler.post(runnable)

    }

    override fun onInsertAllArticlesSuccess(tag: String) {
        Log.d(tag, "onInsertAllArticlesSuccess")
    }

    override fun onWorkerError(error: Throwable, tag: String) {
        Log.e(tag, "onWorkerError")

        when {
            TAG_GET_LIST_ARTICLES_FROM_REMOTE == tag -> {
                //handle error on get list article from remote

                //try to get the articles from databases if possible
                GetAllArticlesWorker(ArticlesDatabase.getInstance(this.interactorContext)!!, this, TAG_GET_LIST_ARTICLES_FROM_LOCAL).execute()
            }
            TAG_GET_LIST_ARTICLES_FROM_LOCAL == tag -> {
                //handle error on get list article from local

                val errorMessage = error.message.toString()

                // Get a handler that can be used to post to the main thread
                val mainHandler = Handler(Looper.getMainLooper())
                val runnable = Runnable {
                    presenter.onFetchDataError(errorMessage)
                }
                mainHandler.post(runnable)
            }

        }
    }

    override fun onTokenInvalid() {
        //nothing here
    }
}
package com.smess.kotlinmvpdemoapp.scenes.articleDetail

import com.smess.basemvpandroidarchitecture.ui.BaseInteractor


/**
 * Created by samy.messara
 */
class ArticleDetailInteractor(presenter: ArticleDetailPresenter) : BaseInteractor<ArticleDetailContract.PresenterContract.Interactor>(presenter), ArticleDetailContract.InteractorContract {

}
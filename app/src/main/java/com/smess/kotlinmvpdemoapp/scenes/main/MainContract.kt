package com.smess.kotlinmvpdemoapp.scenes.main

import android.content.Context
import android.support.v4.widget.SwipeRefreshLayout
import android.support.v7.widget.RecyclerView
import com.smess.basemvpandroidarchitecture.ui.MVPContract
import com.smess.kotlinmvpdemoapp.domain.models.Article
import com.smess.kotlinmvpdemoapp.scenes.main.adapter.ListArticleAdapter


interface MainContract {

    interface ViewContract : MVPContract.ViewContract {
        //functions implemented by the view and called from the presenter goes here

        fun refreshListArticlesContent(adapter: ListArticleAdapter)
        fun stopRefreshing()
        fun showProgress()
        fun hideProgress()
        fun showNoDataLayout()
        fun hideNoDataLayout()
        fun showSnackBarMessage(message: String)
        fun hideListArticles()
        fun showListArticles()
        fun smoothScrollToTop()
        fun hideFab()
        fun showFab()
        fun goToArticleDetailActivity(article: Article)

    }

    interface PresenterContract : MVPContract.PresenterContract {

        interface View : PresenterContract, SwipeRefreshLayout.OnRefreshListener {
            //functions implemented by the presenter and called from the view goes here 

            fun loadData()
            fun setContext(applicationContext: Context?)
            fun onClickFabScrollTop()
            fun onScrollStateChanged(recyclerView: RecyclerView)
        }

        interface Interactor : PresenterContract {
            //functions implemented by the presenter and called from the interactor goes here

            fun handleListArticles(listArticles: ArrayList<Article>)
            fun onFetchDataError(error: String)
            fun onNoNetworkAvailable(noNetworkErrorMessage: String)

        }
    }

    interface InteractorContract : MVPContract.InteractorContract {
        //functions implemented by the interactor and called from the presenter goes here 

        fun setContext(context: Context?)
        fun getListArticles()
    }
}
package com.smess.kotlinmvpdemoapp.services.article

import com.smess.kotlinmvpdemoapp.BuildConfig
import com.smess.kotlinmvpdemoapp.domain.models.Article
import io.reactivex.Observable
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET
import java.util.concurrent.TimeUnit


/**
 * Created by samy.messara
 */
interface ArticleServices {

    @GET(BuildConfig.TARGET_ARTICLES_SERVICE_PHOTOS)
    fun getArticles(): Observable<ArrayList<Article>>

    companion object {
        private const val URL = BuildConfig.ARTICLES_SERVICE_BASE_ULR

        fun create(): ArticleServices {
            val okHttpClient = OkHttpClient.Builder()
                    .connectTimeout(5, TimeUnit.SECONDS)
                    .writeTimeout(5, TimeUnit.SECONDS)
                    .readTimeout(5, TimeUnit.SECONDS)
                    .build()

            val retrofit = Retrofit.Builder()
                    .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                    .addConverterFactory(GsonConverterFactory.create())
                    .baseUrl(URL)
                    .client(okHttpClient)
                    .build()

            return retrofit.create(ArticleServices::class.java)
        }
    }
}
package com.smess.kotlinmvpdemoapp.storage.article

import android.arch.persistence.room.Database
import android.arch.persistence.room.Room
import android.arch.persistence.room.RoomDatabase
import android.content.Context
import com.smess.kotlinmvpdemoapp.domain.models.Article

/**
 * Created by samy.messara
 */
@Database(entities = [(Article::class)], version = 1, exportSchema = false)
abstract class ArticlesDatabase : RoomDatabase() {

    abstract fun articleStorageDao(): ArticleDao

    companion object {
        private var INSTANCE: ArticlesDatabase? = null

        fun getInstance(context: Context?): ArticlesDatabase? {
            if (INSTANCE == null) {
                synchronized(ArticlesDatabase::class) {
                    INSTANCE = Room.databaseBuilder(context!!.applicationContext,
                            ArticlesDatabase::class.java, "articles.db")
                            .build()
                }
            }
            return INSTANCE
        }

        fun destroyInstance() {
            INSTANCE = null
        }
    }
}
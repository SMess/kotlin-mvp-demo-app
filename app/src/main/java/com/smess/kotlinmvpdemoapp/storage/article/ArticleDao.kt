package com.smess.kotlinmvpdemoapp.storage.article

import android.arch.persistence.room.Dao
import android.arch.persistence.room.Insert
import android.arch.persistence.room.OnConflictStrategy
import android.arch.persistence.room.Query
import com.smess.kotlinmvpdemoapp.domain.models.Article


/**
 * Created by samy.messara
 */
@Dao
interface ArticleDao {

    @Query("select * from articles")
    fun getAllArticles(): List<Article>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertAllArticles(article: List<Article>)

    @Query("DELETE FROM articles")
    fun clearTable()
}
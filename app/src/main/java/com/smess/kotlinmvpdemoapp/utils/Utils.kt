package com.smess.kotlinmvpdemoapp.utils

import android.content.Context
import android.net.ConnectivityManager
import android.support.constraint.ConstraintLayout
import android.support.design.widget.Snackbar
import android.support.v4.content.ContextCompat
import android.widget.TextView
import com.smess.kotlinmvpdemoapp.R


/**
 * Created by samy.messara
 */
object Utils {

    /**
     * Check if the device has internet connection
     *
     * @param context
     *
     * @return true is network available, else false
     */
    fun isNetworkAvailable(context: Context): Boolean {
        val connectivityManager = context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        val activeNetworkInfo = connectivityManager.activeNetworkInfo
        return activeNetworkInfo != null && activeNetworkInfo.isConnected
    }


    /**
     * Display snackbar with message and button
     *
     * @param parentContainer
     * @param error
     * @param buttonText
     */
    fun displayErrorSnackbar(context: Context, parentContainer: ConstraintLayout, error: String, buttonText: String) {
        val snackBar = Snackbar.make(parentContainer, error, Snackbar.LENGTH_LONG)
        snackBar.setAction(buttonText, { snackBar.dismiss() })
        // Changing button text color
        snackBar.setActionTextColor(ContextCompat.getColor(context, R.color.colorPrimary))

        // Changing message text color
        val sbView = snackBar.view
        val textView = sbView.findViewById(android.support.design.R.id.snackbar_text) as TextView
        textView.setTextColor(ContextCompat.getColor(context, R.color.colorPrimary))
        snackBar.show()
    }

}
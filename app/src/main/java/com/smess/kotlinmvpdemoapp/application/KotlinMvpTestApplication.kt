package com.smess.kotlinmvpdemoapp.application

import android.app.Application
import com.smess.kotlinmvpdemoapp.storage.article.ArticlesDatabase

/**
 * Created by samy.messara
 */
class KotlinMvpTestApplication : Application() {

    override fun onCreate() {
        super.onCreate()

        initDatabase()
    }

    private fun initDatabase(){
        //init database
        ArticlesDatabase.getInstance(applicationContext)
    }
}
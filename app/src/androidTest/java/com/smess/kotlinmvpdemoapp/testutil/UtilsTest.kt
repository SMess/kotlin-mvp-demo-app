package com.smess.kotlinmvpdemoapp.testutil

import android.content.Context
import android.support.test.espresso.UiController
import android.support.test.espresso.ViewAction
import android.support.test.espresso.matcher.ViewMatchers.isRoot
import android.view.View
import org.hamcrest.Matcher
import java.io.IOException
import java.nio.charset.Charset


/**
 * Created by samy.messara
 */

object UtilsTest {

    /**
     * Perform action of waiting for a specific time.
     *
     * @param millis
     */
    fun waitFor(millis: Long): ViewAction {
        return object : ViewAction {
            override fun getConstraints(): Matcher<View> {
                return isRoot()
            }

            override fun getDescription(): String {
                return "Wait for $millis milliseconds."
            }

            override fun perform(uiController: UiController, view: View) {
                uiController.loopMainThreadForAtLeast(millis)
            }
        }
    }

    /**
     * read json file from assets and return it as String
     *
     * @param context
     * @param fileName
     */
    fun readJsonFromAssets(context: Context, fileName: String): String? {
        var json: String? = null

        try {
            val inputStream = context.assets.open(fileName)
            val size = inputStream.available()
            val buffer = ByteArray(size)
            inputStream.read(buffer)
            inputStream.close()
            json = String(buffer, Charset.forName("UTF-8"))

        } catch (e: IOException) {
            e.printStackTrace()
        }

        return json
    }

}

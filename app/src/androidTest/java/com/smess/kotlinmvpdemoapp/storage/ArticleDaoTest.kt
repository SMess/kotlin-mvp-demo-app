package com.smess.kotlinmvpdemoapp.storage

import android.arch.persistence.room.Room
import android.support.test.InstrumentationRegistry
import android.support.test.runner.AndroidJUnit4
import com.smess.kotlinmvpdemoapp.domain.models.Article
import com.smess.kotlinmvpdemoapp.storage.article.ArticleDao
import com.smess.kotlinmvpdemoapp.storage.article.ArticlesDatabase
import org.junit.After
import org.junit.Assert
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import java.util.*

/**
 * Created by samy.messara
 */

@RunWith(AndroidJUnit4::class)
class ArticleDaoTest {

    private lateinit var articleDao: ArticleDao
    private lateinit var articlesDatabase: ArticlesDatabase


    @Before
    fun setup() {
        val context = InstrumentationRegistry.getTargetContext()
        articlesDatabase = Room.inMemoryDatabaseBuilder(context, ArticlesDatabase::class.java).build()
        articleDao = articlesDatabase.articleStorageDao()
    }

    @After
    fun tearDown() {
        articlesDatabase.close()
    }

    @Test
    fun testInsertedAndRetrievedArticlesMatch() {
        val articles = ArrayList<Article>()
        articles.add(Article(1, 1, "title1", "http://test.url.com", "http://test.thumbnail.com"))
        articles.add(Article(2, 2, "title2", "http://test.url.com", "http://test.thumbnail.com"))
        articles.add(Article(3, 3, "title3", "http://test.url.com", "http://test.thumbnail.com"))

        articleDao.insertAllArticles(articles)

        val allArticles = articleDao.getAllArticles() as ArrayList<Article>

        Assert.assertEquals(articles.size, allArticles.size)
        Assert.assertEquals(articles.toString(), allArticles.toString())

    }

}
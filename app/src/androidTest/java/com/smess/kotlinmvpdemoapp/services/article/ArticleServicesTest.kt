package com.smess.kotlinmvpdemoapp.services.article

import android.support.test.InstrumentationRegistry
import android.support.test.runner.AndroidJUnit4
import com.google.gson.GsonBuilder
import com.smess.kotlinmvpdemoapp.domain.models.Article
import com.smess.kotlinmvpdemoapp.testutil.UtilsTest
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import org.junit.Assert
import org.junit.Test
import org.junit.runner.RunWith


/**
 * Created by samy.messara
 */
@RunWith(AndroidJUnit4::class)
class ArticleServicesTest {

    private var disposable: Disposable? = null
    private var context = InstrumentationRegistry.getTargetContext()


    @Test
    fun testGetArticlesService() {


        var strJsonLocalListArticles = UtilsTest.readJsonFromAssets(context, "articles.json")

        val gson = GsonBuilder().create()
        val localListArticles: List<Article> =  gson.fromJson(strJsonLocalListArticles, Array<Article>::class.java).asList()


        var remoteListArticles: ArrayList<Article>
        disposable = ArticleServices.create().getArticles()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                        { result ->
                            remoteListArticles = result
                            Assert.assertEquals(remoteListArticles.toString(), localListArticles.toString())
                            disposable?.dispose()
                        },
                        { error ->
                            AssertionError()
                            disposable?.dispose()
                        }
                )
    }
}
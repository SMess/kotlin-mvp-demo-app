package com.smess.kotlinmvpdemoapp.scenes.main

import android.content.Intent
import android.content.pm.ActivityInfo
import android.support.test.espresso.Espresso
import android.support.test.espresso.Espresso.onView
import android.support.test.espresso.action.ViewActions.click
import android.support.test.espresso.assertion.ViewAssertions
import android.support.test.espresso.assertion.ViewAssertions.matches
import android.support.test.espresso.contrib.RecyclerViewActions
import android.support.test.espresso.matcher.ViewMatchers
import android.support.test.espresso.matcher.ViewMatchers.*
import android.support.test.rule.ActivityTestRule
import android.support.test.runner.AndroidJUnit4
import android.support.v7.widget.RecyclerView
import com.smess.kotlinmvpdemoapp.R
import com.smess.kotlinmvpdemoapp.scenes.main.MainActivity
import com.smess.kotlinmvpdemoapp.testutil.RecyclerViewMatcher
import com.smess.kotlinmvpdemoapp.testutil.UtilsTest.waitFor
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith

/**
 * Created by samy.messara
 */
@RunWith(AndroidJUnit4::class)
class MainActivityTest {

    @Rule
    @JvmField
    var activityRule = ActivityTestRule(MainActivity::class.java, true, true)

    private fun withRecyclerView(recyclerViewId: Int): RecyclerViewMatcher {
        return RecyclerViewMatcher(recyclerViewId)
    }

    @Test
    fun testRecyclerViewShowingCorrectItems() {

        activityRule.launchActivity(Intent())

        onView(isRoot()).perform(waitFor(3000))

        checkTitleOnPosition(0, "accusamus beatae ad facilis cum similique qui sunt")
        checkTitleOnPosition(1, "reprehenderit est deserunt velit ipsam")
    }

    @Test
    fun testKeepStateOnScreenRotation() {

        activityRule.launchActivity(Intent())

        //Some delay for view creation and loading
        onView(isRoot()).perform(waitFor(3000))

        // Scroll
        Espresso.onView(ViewMatchers.withId(R.id.rvListArticles)).perform(RecyclerViewActions.scrollToPosition<RecyclerView.ViewHolder>(8))

        activityRule.activity.requestedOrientation = ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE

        //Some delay for view creation and loading
        onView(isRoot()).perform(waitFor(3000))

        checkTitleOnPosition(8, "qui eius qui autem sed")
    }

    @Test
    fun testActivityChangeOnItemClick() {

        activityRule.launchActivity(Intent())

        //Some delay for view creation and loading
        onView(isRoot()).perform(waitFor(3000))

        Espresso.onView(ViewMatchers.withId(R.id.rvListArticles)).perform(click())

        onView(withId(R.id.ivArticleDetail)).perform(click()).check(matches(isDisplayed()))
    }

    @Test
    fun testTopScrollOnFabClick(){

    }

    private fun checkTitleOnPosition(position: Int, expectedTitle: String) {
        Espresso.onView(withRecyclerView(R.id.rvListArticles).atPositionOnView(position, R.id.tvTitle)).check(ViewAssertions.matches(ViewMatchers.withText(expectedTitle)))
    }
}
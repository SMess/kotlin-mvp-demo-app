package com.smess.kotlinmvpdemoapp.scenes.articleDetail

import android.content.Intent
import android.support.test.espresso.Espresso
import android.support.test.espresso.action.ViewActions
import android.support.test.espresso.assertion.ViewAssertions
import android.support.test.espresso.matcher.ViewMatchers
import android.support.test.rule.ActivityTestRule
import android.support.test.runner.AndroidJUnit4
import com.smess.kotlinmvpdemoapp.R
import com.smess.kotlinmvpdemoapp.testutil.UtilsTest
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith

/**
 * Created by samy.messara
 */
@RunWith(AndroidJUnit4::class)
class ArticleDetailActivityTest {

    @Rule
    @JvmField
    var activityRule = ActivityTestRule(ArticleDetailActivity::class.java, true, true)

    @Test
    fun testActivityShowingArticle() {
        activityRule.launchActivity(Intent())

        //Some delay for view creation and loading
        Espresso.onView(ViewMatchers.isRoot()).perform(UtilsTest.waitFor(1500))

        Espresso.onView(ViewMatchers.withId(R.id.ivArticleDetail)).perform(ViewActions.click()).check(ViewAssertions.matches(ViewMatchers.isDisplayed()))

    }

}
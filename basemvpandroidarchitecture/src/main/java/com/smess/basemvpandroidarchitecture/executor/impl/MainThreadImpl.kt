package com.smess.basemvpandroidarchitecture.executor.impl

import android.os.Handler
import android.os.Looper
import com.smess.basemvpandroidarchitecture.executor.MainThread

/**
 * This class makes sure that the runnable we provide will be run on the main UI thread.
 *
 * Created by samy.messara
 */

class MainThreadImpl : MainThread {

    private val mHandler: Handler = Handler(Looper.getMainLooper())

    override fun post(runnable: Runnable) {
        mHandler.post(runnable)
    }

    companion object {

        private var sMainThread: MainThread? = null

        val instance: MainThread
            get() {
                if (sMainThread == null) {
                    sMainThread = MainThreadImpl()
                }

                return sMainThread as MainThread
            }
    }
}

package com.smess.basemvpandroidarchitecture.workers

/**
 * Created by samy.messara
 */

interface BaseWorkerCallback {
    fun onWorkerError(error: Throwable, tag: String)
    fun onTokenInvalid()
}
